﻿using System;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Utility to call a function if a gameobject collides with a trigger in the layer
/// </summary>
public class LayerDetectionController : MonoBehaviour
{
    public LayerMask layer;

    /// <summary>
    /// Class to add a function as a serialized field
    /// </summary>
    [Serializable]
    public class CallbackFunction : UnityEvent<bool, GameObject> { }

    public CallbackFunction callbackFunction;

    // Called when the trigger is colliding
    void OnTriggerEnter(Collider collision)
    {
        // Check if the collision is on the right layer
        if (collision.gameObject.layer == Mathf.Log(layer.value, 2))
        {
            callbackFunction.Invoke(true, collision.gameObject);
        }
    }

    // Called when the trigger is no longer colliding
    void OnTriggerExit(Collider collision)
    {
        // Check if the collision is on the right layer
        if (collision.gameObject.layer == Mathf.Log(layer.value, 2))
        {
            callbackFunction.Invoke(false, collision.gameObject);
        }
    }
}
