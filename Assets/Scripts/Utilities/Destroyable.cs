﻿using UnityEngine;
/// <summary>
/// A class used to expose a function allowing to destroy the gameObject it's attached to
/// </summary>
public class Destroyable : MonoBehaviour
{
    public void DestroyMe()
    {
        Destroy(gameObject);
    }
}
