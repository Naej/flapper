﻿using UnityEngine;

/// <summary>
/// This script makes the gameObject go in one direction
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class DirectionnalMovement : MonoBehaviour
{
    /// <value>The speed and direction of the gameObject</value>
    public Vector3 speed;

    private Rigidbody myRigidBody;
    
    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody>();
        myRigidBody.velocity = speed;
    }
}
