﻿using UnityEngine;
/// <summary>
/// This script makes the gameObject able to flap
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class FlapMovement : MonoBehaviour
{
    private Rigidbody myRigidbody;

    /// <value>The speed and direction of the flap</value>
    public Vector3 flapForce;

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Make the gameObject flap
    /// </summary>
    public void Flap()
    {
        if (enabled)
        {
            myRigidbody.velocity = flapForce;
        }
    }
}
