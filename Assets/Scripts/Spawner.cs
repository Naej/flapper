﻿using UnityEngine;

/// <summary>
/// Class used to spawn the obstacles
/// </summary>
public class Spawner : MonoBehaviour
{
    /// <value>the gameObject that will be instantiated</value>
    public GameObject toSpawn;
    /// <value>The range of Y values the Spawner randomly used to spawn the object</value>
    public float randomRange;

    /// <summary>
    /// Instantiate the toSpawn object within [-randomRange,+randomRange] y values
    /// </summary>
    public void Spawn()
    {
        var current = Instantiate(toSpawn,transform);
        current.transform.position = new Vector3(current.transform.position.x,Random.Range(-randomRange, randomRange),current.transform.position.y);
    }
}
